package com.cbalcazar.pragma.mc.person.infrastructure.mappers;

import com.cbalcazar.pragma.mc.person.domain.models.City;
import com.cbalcazar.pragma.mc.person.infrastructure.persistencia.entities.CityEntity;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ICityMapper {

    City cityEntityToCity(CityEntity cityEntity);

    CityEntity cityToCityEntity(City city);

}
