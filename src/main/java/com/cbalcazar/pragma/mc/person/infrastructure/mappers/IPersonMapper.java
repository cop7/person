package com.cbalcazar.pragma.mc.person.infrastructure.mappers;

import com.cbalcazar.pragma.mc.person.application.dtos.PersonDto;
import com.cbalcazar.pragma.mc.person.domain.models.Person;
import com.cbalcazar.pragma.mc.person.infrastructure.persistencia.entities.PersonEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;


@Mapper(componentModel = "spring", uses = {ITypesDocumentMapper.class, ICityMapper.class})
@WithoutMetadata
public interface IPersonMapper {

    Person personEntityToPerson(PersonEntity personEntity);

    PersonEntity personToPersonEntity(Person person);

    List<Person> personEntitiesToPersons(List<PersonEntity> personEntities);

}
