package com.cbalcazar.pragma.mc.person.infrastructure.persistencia;

import com.cbalcazar.pragma.mc.person.infrastructure.persistencia.entities.PersonEntity;
import com.cbalcazar.pragma.mc.person.infrastructure.persistencia.entities.TypesDocumentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface PersonJPARepository extends JpaRepository<PersonEntity, Long> {

    Optional<PersonEntity> findTopByTypesDocumentsAndDocumentNumber(TypesDocumentEntity typesDocuments, String documentNumber);

    List<PersonEntity> findByBirthDateLessThanEqual(LocalDate date);

    Optional<PersonEntity> findByDocumentNumber(String documentNumber);

}
