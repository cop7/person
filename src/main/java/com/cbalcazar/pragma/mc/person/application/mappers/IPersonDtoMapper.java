package com.cbalcazar.pragma.mc.person.application.mappers;

import com.cbalcazar.pragma.mc.person.application.dtos.PersonDto;
import com.cbalcazar.pragma.mc.person.domain.models.Person;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {ITypesDocumentDtoMapper.class, ICityDtoMapper.class})
public interface IPersonDtoMapper {

    PersonDto personToPersonDto(Person person);

    Person personDtoToPerson(PersonDto person);

    List<PersonDto> personsToPersonsDto(List<Person> persons);

}
