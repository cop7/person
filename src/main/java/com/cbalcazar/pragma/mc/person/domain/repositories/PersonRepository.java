package com.cbalcazar.pragma.mc.person.domain.repositories;

import com.cbalcazar.pragma.mc.person.domain.models.Person;

import java.time.LocalDate;
import java.util.List;

public interface PersonRepository {

    List<Person> getAll();

    Person getPersonById(Long id);

    Person getPersonByTypeAndDocument(Long typeDocument, String documentNumber);

    List<Person> getOlderPersons(LocalDate date);

    Long insert(Person person);

    Long update(Person person);

    String delete(Long id);

    Person findByDocumentNumber(String documentNumber);
}
