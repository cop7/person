package com.cbalcazar.pragma.mc.person.infrastructure.mappers;

import com.cbalcazar.pragma.mc.person.domain.models.TypesDocument;
import com.cbalcazar.pragma.mc.person.infrastructure.persistencia.entities.TypesDocumentEntity;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ITypesDocumentMapper {

    TypesDocument typesDocumentTypesDocumentDto(TypesDocumentEntity typesDocumentEntity);

    TypesDocumentEntity typesDocumentDtoToTypesDocument(TypesDocument typesDocument);

}
