package com.cbalcazar.pragma.mc.person.infrastructure.mappers;

import org.mapstruct.Mapping;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.CLASS)
@Mapping(target = "image", ignore = true)
public @interface WithoutMetadata {
}
