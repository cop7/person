package com.cbalcazar.pragma.mc.person.infrastructure.exceptions;

public class BusinnessPersonException extends RuntimeException {

    public BusinnessPersonException() {
        super();
    }

    public BusinnessPersonException(String message) {
        super(message);
    }

    public BusinnessPersonException(String message, Throwable cause) {
        super(message, cause);
    }
}
