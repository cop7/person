package com.cbalcazar.pragma.mc.person.infrastructure.persistencia;

import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;

import com.cbalcazar.pragma.mc.person.domain.models.Person;
import com.cbalcazar.pragma.mc.person.domain.repositories.PersonRepository;
import com.cbalcazar.pragma.mc.person.infrastructure.exceptions.BusinnessPersonException;
import com.cbalcazar.pragma.mc.person.infrastructure.feigns.ImageFeign;
import com.cbalcazar.pragma.mc.person.infrastructure.mappers.IPersonMapper;
import com.cbalcazar.pragma.mc.person.infrastructure.persistencia.entities.PersonEntity;
import com.cbalcazar.pragma.mc.person.infrastructure.persistencia.entities.TypesDocumentEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class PersonJpaAdapter implements PersonRepository {

    @Autowired
    private PersonJPARepository personJPARepository;

    @Autowired
    private IPersonMapper iPersonMapper;

    @Autowired
    CircuitBreakerFactory factory;


    @Autowired
    private ImageFeign imageFeign;

    @Override
    public List<Person> getAll() {
        return iPersonMapper.personEntitiesToPersons(personJPARepository.findAll());
    }

    @Override
    public Person getPersonById(Long id) {

        PersonEntity personEntity = personJPARepository
                .findById(id)
                .orElseThrow(
                        () -> new BusinnessPersonException(String.format("Person with id number %s was not found", id))
                );

        return iPersonMapper.personEntityToPerson(personEntity);
    }

    @Override
    public Person getPersonByTypeAndDocument(Long typeDocument, String documentNumber) {

        TypesDocumentEntity typesDocument = TypesDocumentEntity.builder()
                .id(typeDocument)
                .build();

        PersonEntity person = personJPARepository.findTopByTypesDocumentsAndDocumentNumber(
                typesDocument, documentNumber
        ).orElseThrow(() -> new BusinnessPersonException(String.format("Person with document number %s was not found", documentNumber)));

        return iPersonMapper.personEntityToPerson(person);
    }

    @Override
    public List<Person> getOlderPersons(LocalDate date) {

        return iPersonMapper.personEntitiesToPersons(personJPARepository.findByBirthDateLessThanEqual(date));
    }

    @Override
    public Long insert(Person person) {

        PersonEntity personEntity = iPersonMapper.personToPersonEntity(person);

        personJPARepository.save(personEntity);

        CircuitBreaker circuitBreaker = factory.create("circuitId");

        Long imageId = circuitBreaker.run(
                () -> imageFeign.saveImage(person.getImage()).getBody(),
                t -> null);

        personEntity.setImageId(imageId);

        return personEntity.getId();
    }

    @Override
    public Long update(Person person) {

        PersonEntity personEntity = iPersonMapper.personToPersonEntity(person);

        personJPARepository.save(personEntity);

        CircuitBreaker circuitBreaker = factory.create("circuitId");

        Long imageId = circuitBreaker.run(
                () -> imageFeign.saveImage(person.getImage()).getBody(),
                t -> null);

        personEntity.setImageId(imageId);

        return personEntity.getId();
    }

    @Override
    public String delete(Long id) {
        return null;
    }

    @Override
    public Person findByDocumentNumber(String documentNumber) {

        PersonEntity person = personJPARepository.findByDocumentNumber(documentNumber).orElseThrow(
                () -> new BusinnessPersonException(
                        String.format("There is already a person with document number %s", documentNumber)
                )
        );

        return iPersonMapper.personEntityToPerson(person);
    }
}
