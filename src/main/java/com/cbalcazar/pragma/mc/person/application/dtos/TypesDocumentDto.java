package com.cbalcazar.pragma.mc.person.application.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TypesDocumentDto {

    @NotEmpty
    private Integer id;
    private String name;
}
