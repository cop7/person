package com.cbalcazar.pragma.mc.person.infrastructure.configurations;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreakerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigBuilder;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class Resilience4jConfig {

    CircuitBreakerConfig circuitBreakerConfig = CircuitBreakerConfig.custom()
            .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
            .slidingWindowSize(6)
            .failureRateThreshold(50)
            .waitDurationInOpenState(Duration.ofMillis(40000))
            .build();

    @Bean
    public Customizer<Resilience4JCircuitBreakerFactory> globalResilience4JCircuitBreakerFactoryCustomizer() {
        // configuración global
        return factory -> factory.configureDefault(id -> new Resilience4JConfigBuilder(id)
                .circuitBreakerConfig(circuitBreakerConfig)
                .build());

        //configuración específica para un determinado circuit breaker
        /*return factory -> factory.configure(builder -> builder
          		  .circuitBreakerConfig(config)
        	      .build(), "circuit1");*/
    }

}
