package com.cbalcazar.pragma.mc.person.infrastructure.endpoints;

import com.cbalcazar.pragma.mc.person.application.dtos.PersonDto;
import com.cbalcazar.pragma.mc.person.application.handlers.PersonHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@CrossOrigin("*")
public class PersonEndPoint {

    @Value("${eureka.instance.instance-id}")
    String id;

    private static final Logger logger = LogManager.getLogger(PersonEndPoint.class);

    @Autowired
    private PersonHandler clienteHandler;

    @Operation(summary = "Get all person")
    @GetMapping("/")
    public ResponseEntity<List<PersonDto>> allPerson() {
        logger.info("allPerson");
        return ResponseEntity.status(HttpStatus.OK).body(clienteHandler.getAll());
    }

    @Operation(summary = "Get a person by its id")
    @GetMapping("/{id}")
    public ResponseEntity<PersonDto> onePerson(
            @Parameter(description = "id of person to be searched")
            @PathVariable("id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(clienteHandler.getPersonById(id));
    }

    @Operation(summary = "Get a person by document type and document number")
    @GetMapping("/{type_document}/{document_number}")
    public ResponseEntity<PersonDto> getPersonByTipoAndDocument(
            @PathVariable("type_document") Long typeDocument,
            @PathVariable("document_number") String documentNumber
    ) {
        return ResponseEntity.status(HttpStatus.OK).body(clienteHandler.getPersonByTypeAndDocument(
                typeDocument,
                documentNumber
        ));
    }

    @Operation(summary = "Get older persons at a certain age")
    @GetMapping("/older/{years}")
    public ResponseEntity<List<PersonDto>> getOlderPersons(@PathVariable("years") Integer years) {
        return ResponseEntity.status(HttpStatus.OK).body(clienteHandler.getOlderPersons(years));
    }

    @PostMapping(value = "/")
    @Operation(summary = "Create a person", description = "The field with name person must be the json of PersonEro")
    public ResponseEntity<Long> addPerson(@RequestBody PersonDto personDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(clienteHandler.insert(personDto));
    }

    @PutMapping("/")
    public ResponseEntity<Long> updatePerson(@RequestBody PersonDto personDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(clienteHandler.update(personDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePerson(@PathVariable("id") Long id) {
        return new ResponseEntity(clienteHandler.delete(id), HttpStatus.OK);
    }

}
