package com.cbalcazar.pragma.mc.person.domain.usecases;

import com.cbalcazar.pragma.mc.person.domain.models.Person;
import com.cbalcazar.pragma.mc.person.domain.repositories.PersonRepository;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
public class PersonUseCase {

    private final PersonRepository personRepository;

    public List<Person> getAll() {
        return personRepository.getAll();
    }


    public Person getPersonById(Long id) {
        return personRepository.getPersonById(id);
    }


    public Person getPersonByTypeAndDocument(Long typeDocument, String documentNumber) {
        //Logic
        return personRepository.getPersonByTypeAndDocument(typeDocument, documentNumber);
    }


    public List<Person> getOlderPersons(Integer years) {
        //Logic
        LocalDate date = LocalDate.now().minusYears(years);

        return personRepository.getOlderPersons(date);
    }


    public Long insert(Person person) {
        //Logic
        person.setId(null);

        personRepository.findByDocumentNumber(person.getDocumentNumber());

        person.setId(personRepository.insert(person));

        return person.getId();
    }


    public Long update(Person person) {
        //Logic
        Long personId = person.getId();

        Person personOld = this.getPersonById(personId);

        if (!personOld.getDocumentNumber().equals(person.getDocumentNumber())) {
            personRepository.findByDocumentNumber(person.getDocumentNumber());
        }

        return personRepository.update(person);
    }


    public String delete(Long id) {
        //Logic
        return personRepository.delete(id);
    }

}
