package com.cbalcazar.pragma.mc.person.application.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.context.annotation.Description;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class PersonDto implements Serializable {

    private Long id;
    @NotEmpty
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("type_document")
    private TypesDocumentDto typesDocuments;
    @NotEmpty
    @JsonProperty("document_number")
    private String documentNumber;
    @JsonProperty("birth_date")
    private LocalDate birthDate;
    @JsonProperty("city")
    private CityDto cities;
    @Schema(name = "image",
            description = "Image base64")
    private String image;
}
