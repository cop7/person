package com.cbalcazar.pragma.mc.person.infrastructure.configurations;

import com.cbalcazar.pragma.mc.person.application.handlers.PersonHandler;
import com.cbalcazar.pragma.mc.person.application.mappers.IPersonDtoMapper;
import com.cbalcazar.pragma.mc.person.domain.repositories.PersonRepository;
import com.cbalcazar.pragma.mc.person.domain.usecases.PersonUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguracion {

    @Bean
    public PersonUseCase personUseCase(PersonRepository personRepository) {
        return new PersonUseCase(personRepository);
    }

    @Bean
    public PersonHandler clienteHandler(PersonUseCase clienteUseCase, IPersonDtoMapper iPersonMapper) {
        return new PersonHandler(clienteUseCase, iPersonMapper);
    }

}
