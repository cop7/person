package com.cbalcazar.pragma.mc.person.application.handlers;

import com.cbalcazar.pragma.mc.person.application.dtos.PersonDto;
import com.cbalcazar.pragma.mc.person.application.mappers.IPersonDtoMapper;
import com.cbalcazar.pragma.mc.person.domain.usecases.PersonUseCase;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class PersonHandler {

    private final PersonUseCase personUseCase;

    private final IPersonDtoMapper iPersonMapper;

    public List<PersonDto> getAll() {
        return iPersonMapper.personsToPersonsDto(personUseCase.getAll());
    }


    public PersonDto getPersonById(Long id) {
        return iPersonMapper.personToPersonDto(personUseCase.getPersonById(id));
    }


    public PersonDto getPersonByTypeAndDocument(Long typeDocument, String documentNumber) {
        return iPersonMapper.personToPersonDto(personUseCase.getPersonByTypeAndDocument(typeDocument, documentNumber));
    }


    public List<PersonDto> getOlderPersons(Integer years) {
        return iPersonMapper.personsToPersonsDto(personUseCase.getOlderPersons(years));
    }


    public Long insert(PersonDto person) {
        return personUseCase.insert(iPersonMapper.personDtoToPerson(person));
    }


    public Long update(PersonDto person) {
        return personUseCase.update(iPersonMapper.personDtoToPerson(person));
    }


    public String delete(Long id) {
        return personUseCase.delete(id);
    }
}
