package com.cbalcazar.pragma.mc.person.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TypesDocument {

    private Long id;
    private String name;
}
