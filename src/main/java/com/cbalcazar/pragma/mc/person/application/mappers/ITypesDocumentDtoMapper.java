package com.cbalcazar.pragma.mc.person.application.mappers;

import com.cbalcazar.pragma.mc.person.application.dtos.TypesDocumentDto;
import com.cbalcazar.pragma.mc.person.domain.models.TypesDocument;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ITypesDocumentDtoMapper {

    TypesDocumentDto typesDocumentTypesDocumentDto(TypesDocument typesDocument);

    TypesDocument typesDocumentDtoToTypesDocument(TypesDocumentDto typesDocumentDto);

}
