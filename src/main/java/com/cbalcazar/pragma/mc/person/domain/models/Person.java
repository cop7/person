package com.cbalcazar.pragma.mc.person.domain.models;

import lombok.*;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class Person {

    private Long id;
    private String firstName;
    private String lastName;
    private TypesDocument typesDocuments;
    private String documentNumber;
    private LocalDate birthDate;
    private City cities;
    private String image;
}

