package com.cbalcazar.pragma.mc.person.application.mappers;

import com.cbalcazar.pragma.mc.person.application.dtos.CityDto;
import com.cbalcazar.pragma.mc.person.domain.models.City;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ICityDtoMapper {

    CityDto cityToCityDto(City city);

    City cityDtoToCity(CityDto cityDto);

}
