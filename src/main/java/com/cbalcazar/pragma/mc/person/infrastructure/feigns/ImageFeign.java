package com.cbalcazar.pragma.mc.person.infrastructure.feigns;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "api-image")
public interface ImageFeign {

    @PostMapping(value = "/")
    ResponseEntity<Long> saveImage(@RequestParam String imageBase64);

}
